#include "math/m_space.h"
#include "stereokit.h"
#include "stereokit_ui.h"
#include "xrt/xrt_defines.h"
using namespace sk;

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

struct state {
  bool palm_pose;
};

// Inefficient, but we don't care.
xrt_pose xrt_from_sk(sk::pose_t sk) {
  xrt_pose ret;
  ret.position.x = sk.position.x;
  ret.position.y = sk.position.y;
  ret.position.z = sk.position.z;

  ret.orientation.x = sk.orientation.x;
  ret.orientation.y = sk.orientation.y;
  ret.orientation.z = sk.orientation.z;

  ret.orientation.w = sk.orientation.w;
  return ret;
}

void pose_in_other_pose(xrt_pose base, xrt_pose pose, const char *fmt0,
                        const char *fmt) {
  struct xrt_relation_chain xrc = {};
  m_relation_chain_push_pose(&xrc, &pose);
  m_relation_chain_push_inverted_pose_if_not_identity(&xrc, &base);
  struct xrt_space_relation pose_in_base;
  m_relation_chain_resolve(&xrc, &pose_in_base);

  xrt_pose p = pose_in_base.pose;
  printf("%s %s: position: {x: %f y: %f z: %f}, orientation: {x: %f, y: %f, z: "
         "%f, w: %f}\n",
         fmt0, fmt, p.position.x, p.position.y, p.position.z, p.orientation.x,
         p.orientation.y, p.orientation.z, p.orientation.w);
}

void run(void *ptr) {
  struct state &st = *(state *)ptr;

  sk::handed_ hands[2] = {sk::handed_left, sk::handed_right};
  const char *names[2] = {"Left", "Right"};

  for (int h = 0; h < 2; h++) {
    const sk::controller_t *con = sk::input_controller(sk::handed_left);

    xrt_pose grip = xrt_from_sk(con->pose);
    xrt_pose aim = xrt_from_sk(con->aim);
    xrt_pose palm = xrt_from_sk(con->palm);

    pose_in_other_pose(grip, aim, names[h], "aim pose in grip pose");
    pose_in_other_pose(aim, grip, names[h], "grip pose in aim pose");

    if (st.palm_pose) {
      pose_in_other_pose(palm, aim, names[h], "aim pose in palm pose");
      pose_in_other_pose(palm, grip, names[h], "grip pose in palm pose");

      pose_in_other_pose(aim, palm, names[h], "palm pose in aim pose");
      pose_in_other_pose(grip, palm, names[h], "palm pose in grip pose");
    }
    printf("\n");
  }
    printf("\n\n");
}

int main() {
  sk_settings_t settings = {};
  settings.app_name = "StereoKit C";
  settings.assets_folder = "/2/XR/skNotes/Assets";
  settings.display_preference = display_mode_mixedreality;
  settings.overlay_app = true;
  settings.overlay_priority = 1;
  if (!sk_init(settings))
    return 1;

  struct state &st = *(new state);

  st.palm_pose = backend_openxr_ext_enabled("XR_EXT_palm_pose");

  sk_run_data(run, &st, run, &st);

  return 0;
}
